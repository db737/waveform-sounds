import matplotlib.pyplot as plt
from numpy import pi
import numpy as np

inp = np.loadtxt("Weierstrass.csv", delimiter = ", ")
xs = inp[:, 0]
ys = inp[:, 1]

plt.plot(xs, ys)
plt.show()
