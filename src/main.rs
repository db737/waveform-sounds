extern crate rodio;

use std::thread;
use std::f32::consts::TAU;
use std::fs::File;
use std::io::{self, Write};
use std::time::Duration;
use rodio::{OutputStream, Sample, Sink};
use rodio::source::Source;

#[derive(Clone, Copy, Debug)]
pub enum Shape {
	Blast(f32),
	Saw,
	Sine,
	Square,
	Triangle,
	Weierstrass(f32, u32)
}

impl Shape {
	pub fn name(&self) -> &str {
		match self {
			Self::Blast(_) => "blast",
			Self::Saw => "saw",
			Self::Sine => "sine",
			Self::Square => "square",
			Self::Triangle => "triangle",
			Self::Weierstrass(_, _) => "weierstrass"
		}
	}
}

#[derive(Clone, Copy, Eq, PartialEq)]
enum Mode {
	Normal,
	Play,
	Output
}

impl Shape {
	fn eval(&self, num_sample: usize, freq: f32, sample_rate: u32) -> f32 {
		let sft = num_sample as f32 * freq / sample_rate as f32;
		let sftt = sft * TAU;
		match *self {
			Shape::Blast(s) => (1.0 - sftt / s) * (-sftt / s).exp(),
			Shape::Saw => 2.0 * (sft + 0.5).fract() - 1.0,
			Shape::Sine => sftt.sin(),
			Shape::Square => ((sft + 0.5).fract() - 0.5).signum(),
			Shape::Triangle => {
				let u = (sft + 0.25).fract();
				1.0 - 4.0 * u * u.signum()
			},
			Shape::Weierstrass(a, b) => {
				let mut f = 1.0;
				let mut out = 0.0;
				let mut n = 0;
				while f * freq <= (sample_rate / 2) as f32 {
					out += a.powi(n) * (sftt * f).cos();
					f *= b as f32;
					n += 1
				};
				out * (1.0 - a) / (1.0 - a.powi(n))
			}
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub struct Wave {
	freq: f32,
	num_sample: usize,
	shape: Shape
}

impl Wave {
	#[inline]
	pub fn new(freq: u32, shape: Shape) -> Self {
		Self {
			freq: freq as f32,
			num_sample: 0,
			shape
		}
	}

	pub fn name(&self) -> &str {
		self.shape.name()
	}
}

impl Iterator for Wave {
	type Item = f32;

	#[inline]
	fn next(&mut self) -> Option<f32> {
		let out = self.shape.eval(self.num_sample, self.freq, self.sample_rate());
		self.num_sample = self.num_sample.wrapping_add(1);
		Some(out)
	}
}

impl Source for Wave {
	#[inline]
	fn current_frame_len(&self) -> Option<usize> {
		None
	}

	#[inline]
	fn channels(&self) -> u16 {
		1
	}

	#[inline]
	fn sample_rate(&self) -> u32 {
		48000
	}

	#[inline]
	fn total_duration(&self) -> Option<Duration> {
		None
	}
}

fn play<S>(sink: &Sink, source: S, s: u64)
where <S as Iterator>::Item: Sample + Send, S: 'static + Send + Source {
	let time = Duration::from_secs(s);
	let s = source.take_duration(time);
	sink.append(s.convert_samples::<<S as Iterator>::Item>());
	sink.sleep_until_end()
}

fn pause(s: u64) {
	let time = Duration::from_secs(s);
	thread::sleep(time)
}

fn output(wave: Wave, name: &str) {
	let mut file = File::create(name).unwrap();
	let write = |(i, f): (usize, f32)| writeln!(file, "{}, {}", i, f).unwrap();
	wave.take(300).enumerate().for_each(write)
}

fn main() {
	let (_s, stream_handle) = OutputStream::try_default().unwrap();
	let sink = Sink::try_new(&stream_handle).unwrap();
	let f = |s: Shape| Wave::new(40, s);
	let blast = f(Shape::Blast(100000.0));
	let saw = f(Shape::Saw);
	let sine = f(Shape::Sine);
	let square = f(Shape::Square);
	let triangle = f(Shape::Triangle);
	let weierstrass = f(Shape::Weierstrass(0.80, 7));
	let pp = |w: Wave| {
		play(&sink, w, 2);
		pause(1);
	};
	let mut mode = Mode::Normal;
	loop {
		let mut inp = String::new();
		io::stdin().read_line(&mut inp).unwrap();
		inp = inp.to_lowercase();
		let w = match inp.as_str().trim() {
			"blast" => blast,
			"saw" => saw,
			"sine" => sine,
			"square" => square,
			"triangle" => triangle,
			"weierstrass" => weierstrass,
			"play" => {
				mode = Mode::Play;
				continue
			}
			"output" => {
				mode = Mode::Output;
				continue
			},
			"cancel" => {
				mode = Mode::Normal;
				continue
			}
			"exit" => return,
			_ => {
				println!("Input not recognised");
				continue
			}
		};
		match mode {
			Mode::Normal => println!("No operation chosen"),
			Mode::Play => {
				println!("Now playing {}", &w.name());
				pp(w)
			},
			Mode::Output => {
				let mut fname = w.name().to_string();
				fname.push_str(".csv");
				println!("Writing to {}", &fname);
				output(w, &fname)
			}
		}
	}
}
